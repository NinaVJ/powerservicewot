package com.wotwiki.powerservice.models;

public class Power {

    public enum affinity {
        NONE, FIRE, AIR, WATER, EARTH, SPIRIT
    }

    public enum talent {
        NONE, FORETELLING, DREAMWALKING, HEALING, WEATHER, DELVING, COMPULSION,
        ANGREALMAKING, POWERFORGING, TRAVELING
    }

    private String powerName;
    private boolean truePower;
    private affinity affinity;//private List<affinity> affinities;
    private talent talent;

    public Power(String powerName, boolean truePower, Power.affinity affinity, Power.talent talent) {
        this.powerName = powerName;
        this.truePower = truePower;
        this.affinity = affinity;
        this.talent = talent;
    }

    public String getPowerName() {
        return powerName;
    }

    public void setPowerName(String powerName) {
        this.powerName = powerName;
    }

    public boolean isTruePower() {
        return truePower;
    }

    public void setTruePower(boolean truePower) {
        this.truePower = truePower;
    }

    public Power.affinity getAffinity() {
        return affinity;
    }

    public void setAffinity(Power.affinity affinity) {
        this.affinity = affinity;
    }

    public Power.talent getTalent() {
        return talent;
    }

    public void setTalent(Power.talent talent) {
        this.talent = talent;
    }
}
