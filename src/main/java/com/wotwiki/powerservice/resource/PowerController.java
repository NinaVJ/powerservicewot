package com.wotwiki.powerservice.resource;

import com.wotwiki.powerservice.models.Power;
import com.wotwiki.powerservice.utils.PowerRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController//deze klasse kan bevraagd worden via een HTTP request
@RequestMapping("/power")//When somebody calls power, load this class
public class PowerController {
    /*tell Spring Boot to treat this as an API that is accessible at traits/characterID
        If character/characterId is called, call this method
        PathVariable so that the variable is passed to this method as an argument.
        powerName moet ik uit de path halen in de URL waarop de aanroep binnen komt*/
    //LET OP: METHODE BUITEN KLASSE ZETTEN
    @GetMapping("/{powerName}")//{}tells that it is a variable, not literally powerName
    public Power getPowerByName(@PathVariable("powerName") String powerName){
        List<Power> powerList = PowerRepository.getPowerList();
        for (int i = 0; i < powerList.size(); i++){
            if (powerList.get(i).getPowerName().equals(powerName)){
                return powerList.get(i);
            }
        }
        return null;
    }
}
