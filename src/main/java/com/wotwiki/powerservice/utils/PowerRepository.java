package com.wotwiki.powerservice.utils;

import com.wotwiki.powerservice.models.Power;

import java.util.ArrayList;
import java.util.List;

public class PowerRepository {
    public static List<Power> getPowerList() {

        ArrayList<Power> powerList = new ArrayList<Power>();
        powerList.add(new Power("Saidin", false, Power.affinity.NONE, Power.talent.NONE));
        powerList.add(new Power("Saidar", false, Power.affinity.NONE, Power.talent.NONE));
        powerList.add(new Power("TruePower", true, Power.affinity.NONE, Power.talent.NONE));
        powerList.add(new Power("OnePower", true, Power.affinity.NONE, Power.talent.NONE));
        powerList.add(new Power("Seeing", false, Power.affinity.SPIRIT, Power.talent.FORETELLING));
        powerList.add(new Power("Sniffer", false, Power.affinity.AIR, Power.talent.NONE));
        powerList.add(new Power("Wolfling", false, Power.affinity.NONE, Power.talent.NONE));
        powerList.add(new Power("Ta'veren", false, Power.affinity.NONE, Power.talent.NONE));
        return powerList;
    }
}
